import React, {Component} from 'react';
import './App.css';
import "./scss/MainStyle.scss";
import Header from './components/header/Header';
import PositionLine from './components/positionLine/PositionLine';
import ContactBlock from './components/contactBlock/ContactBlock';
import PartnersCard from './components/partnersCard/PartnersCard';
import Footer from './components/footer/Footer';
import Swiper from 'swiper';

import FirstBlockImage from './images/MainPage/firstBlock.png';
import FirstPrize from './images/MainPage/prizes/firstPrize.svg';
import FirstPrizeCountry from './images/MainPage/prizes/firstPrizeCountry.svg';
import SecondPrize from './images/MainPage/prizes/secondPrize.svg';
import SecondPrizeCountry from './images/MainPage/prizes/secondPrizeCountry.svg';
import ThirdPrize from './images/MainPage/prizes/thirdPrize.svg';
import ThirdPrizeCountry from './images/MainPage/prizes/thirdPrizeCountry.svg';
import ContactPhoto from './images/MainPage/contactPhoto.png';
import LeftBrace from './images/MainPage/contactBlock/leftScobe.svg';
import RightBrace from './images/MainPage/contactBlock/rightScobe.svg';
import FacebookIcon from './images/MainPage/partners/fb.png';
import AmazonIcon from './images/MainPage/partners/amaz.png';
import SonyIcon from './images/MainPage/partners/sony.png';
import AEIcon from './images/MainPage/partners/ae.png';
import {ReactComponent as ArrowBack} from './images/MainPage/arrowBack.svg';
import {ReactComponent as ArrowForward} from './images/MainPage/arrowForward.svg';
import {ReactComponent as PhoneIcon} from './images/Header/PhoneIcon.svg';
import {ReactComponent as InstagramIcon} from './images/Header/instagramIcon.svg';
import {ReactComponent as VkIcon} from './images/Header/vkIcon.svg';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isMenuOpen: false,
            currentSlide: 0,
            slider: null,
        };
        this.openMenu = this.openMenu.bind(this);
        this.handleSwiper = this.handleSwiper.bind(this);
        this.slideBack = this.slideBack.bind(this);
        this.slideForward = this.slideForward.bind(this);
    }

    componentDidMount() {
        let mySwiper = new Swiper('.swiper-container', {spaceBetween: 60});
        mySwiper.on('slideChange', () => this.handleSwiper(mySwiper.activeIndex))
        this.setState({
            slider: mySwiper,
        })
    }

    handleSwiper(id) {
        this.setState({
            currentSlide: id,
        })
    }

    openMenu() {
        const {isMenuOpen} = this.state;
        this.setState({
            isMenuOpen: !isMenuOpen,
        })
    }

    slideBack() {
        const {slider} = this.state;
        slider.slidePrev();
    }

    slideForward() {
        const {slider} = this.state;
        slider.slideNext();
    }

    render() {
        const {isMenuOpen, currentSlide} = this.state;
        return (
            <div className="App">
                <Header openMenu={this.openMenu}/>
                {!!isMenuOpen && isMenuOpen ?
                    <div className="menuWrap">
                        <div className="mobileIcons">
                        <PhoneIcon style={{margin: "0 19px 0 0"}}/>
                        <a href="tel:+4999999999" className="headerPhoneSupport">
                            <span id="headerPhoneSupport">8 499 999 99 99</span>
                        </a>
                        </div>
                        <div className="menuItem">пункт меню 1</div>
                        <div className="menuItem">пункт меню 2</div>
                        <div className="menuItem">пункт меню 3</div>
                        <div className="menuItem">пункт меню 4</div>
                        <div className="menuItem">пункт меню 5</div>
                        <div className="mobileIcons">
                            <VkIcon/>
                            <InstagramIcon style={{margin: "0 0 0 28px"}}/>
                        </div>
                    </div>
                    :
                    null
                }
                <div className="mainPageWrap">
                    <div className="firstBlockWrap">
                        {/*<img className="firstBlockImg" src={FirstBlockImage} alt="Первый блок"/>*/}
                        <div className="textFirstBlock">
                            <div className="title">
                                lorem ipsum dolor sit amet
                            </div>
                            <div className="secondTitle">
                                <span className="secondTitleHighLight" children="lorem "/>
                                ipsum dolor sit amet
                            </div>
                            <div className="prizesButtonAndListMobile">
                            <div className="prizesList">
                                <PositionLine
                                    prize={FirstPrize}
                                    country={FirstPrizeCountry}
                                    words="Lorem ipsum dol"
                                    isGeneral={true}
                                />
                                <PositionLine
                                    prize={SecondPrize}
                                    country={SecondPrizeCountry}
                                    words="lorem ipsum"
                                    isGeneral={false}
                                />
                                <PositionLine
                                    prize={ThirdPrize}
                                    country={SecondPrizeCountry}
                                    words="lorem ipsum dolor sit amet"
                                    isGeneral={false}
                                />
                                <PositionLine
                                    prize={SecondPrize}
                                    country={ThirdPrizeCountry}
                                    words="lorem ipsum"
                                    isGeneral={false}
                                />
                            </div>
                            <div className="prizesButton">
                                <div className="prizesButtonText">
                                    LOREM IPSUM DOLOR
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div className="secondBlockWrap">
                        <div className="secondBlockName">Второй блок</div>
                        <div className="secondBlockContent">
                            <div className="braceBlock">
                                <img className="leftBrace" src={LeftBrace}/>
                            </div>
                            <div className="swiper-container swiper-container-initialized swiper-container-horizontal">
                                <div className="swiper-wrapper">
                                    <div className="swiper-slide">
                                        <ContactBlock image={ContactPhoto}/>
                                    </div>
                                    <div className="swiper-slide">
                                        <ContactBlock image={ContactPhoto}/>
                                    </div>
                                    <div className="swiper-slide">
                                        <ContactBlock image={ContactPhoto}/>
                                    </div>
                                    <div className="swiper-slide">
                                        <ContactBlock image={ContactPhoto}/>
                                    </div>
                                    <div className="swiper-slide">
                                        <ContactBlock image={ContactPhoto}/>
                                    </div>
                                </div>
                            </div>
                            <div className="braceBlock">
                                <img className="rightBrace" src={RightBrace}/>
                            </div>
                        </div>
                        <div className="secondBlockArrows">
                            <ArrowBack onClick={this.slideBack}/>
                            <div className="pagesBlock">
                                {currentSlide + 1} из 5
                            </div>
                            <ArrowForward onClick={this.slideForward}/>
                        </div>
                    </div>
                    <div className="thirdBlockWrap">
                        <div className="thirdBlockName">Третий блок</div>
                        <div className="yearsCollection">
                            <div className="row">
                                <div className="p-0 col-3">
                                    <div className="yearBlock generalYear m-1">2019</div>
                                </div>
                                <div className="p-0 col-3">
                                    <div className="yearBlock  m-1">2018</div>
                                </div>
                                <div className="p-0 col-3">
                                    <div className="yearBlock  m-1">2017</div>
                                </div>
                                <div className="p-0 col-3">
                                    <div className="yearBlock  m-1">2016</div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="p-0 col-3">
                                    <div className="yearBlock  m-1">2015</div>
                                </div>
                                <div className="p-0 col-3">
                                    <div className="yearBlock  m-1">2014</div>
                                </div>
                                <div className="p-0 col-3">
                                    <div className="yearBlock  m-1">2013</div>
                                </div>
                                <div className="p-0 col-3">
                                    <div className="yearBlock  m-1">2012</div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="p-0 col-3">
                                    <div className="yearBlock  m-1">2011</div>
                                </div>
                            </div>
                        </div>
                        <div className="thirdBlockContent">
                            <div className="partnersRow row">
                                <PartnersCard
                                    image={FacebookIcon}
                                    title="Lorem ipsum dolor sit amet."
                                    description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate."
                                />
                                <PartnersCard
                                    image={AmazonIcon}
                                    title="Lorem ipsum dolor sit amet."
                                    description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate."
                                />
                                <PartnersCard
                                    image={SonyIcon}
                                    title="Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet."
                                />
                                <PartnersCard
                                    image={AEIcon}
                                    title="Lorem ipsum dolor sit amet."
                                    description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate."
                                />
                            </div>
                            <div className="partnersRow row">
                                <PartnersCard
                                    image={FacebookIcon}
                                    title="Lorem ipsum dolor sit amet."
                                    description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate."
                                />
                                <PartnersCard
                                    image={AmazonIcon}
                                    title="Lorem ipsum dolor sit amet."
                                    description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate."
                                />
                                <PartnersCard
                                    image={SonyIcon}
                                    title="Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet."
                                />
                                <PartnersCard
                                    image={AEIcon}
                                    title="Lorem ipsum dolor sit amet."
                                    description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate."
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <Footer/>
            </div>
        );
    }
}

export default App;
