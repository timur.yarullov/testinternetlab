import React from "react";

export default class PositionLine extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {prize, country, words, isGeneral} = this.props;
        return(
            <div className="positionLine">
                <div className="prizeBlock">
                    <img src={prize} alt="prize" className="prize"/>
                </div>
                <img src={country} alt="prizeCountry" className="prizeCountry"/>
                {!!isGeneral && isGeneral ?
                    <div className="generalWords words">{words}</div>
                    :
                    <div className="usualWords words">{words}</div>
                }
            </div>
        );
    }
}