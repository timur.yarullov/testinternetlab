import React from "react";

export default class PartnersCard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {image, title, description} = this.props;
        return(
            <div className="partnersCardWrap col-xl-3 p-0 col-md-6 ">
                <div className="partnersCardImageBlock">
                    <img className="partnerImage" src={image}/>
                </div>
                <div className="partnerTitle">
                    {title}
                </div>
                <div className="partnersDescription">
                    {description}
                </div>
            </div>
        );
    }
}