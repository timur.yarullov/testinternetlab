import React from "react";
import CopyRight from "../../images/Footer/copyRight.png";
import Union from "../../images/Footer/Union.svg"

export default class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div className="footerWrap">
                <img src={CopyRight} alt="CopyRight"/>
                <img src={Union} alt="Union"/>
            </div>
        );
    }
}