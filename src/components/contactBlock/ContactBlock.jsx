import React from "react";

export default class ContactBlock extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {image} = this.props;

        const contactAchieveList = [
            "lorem ipsum dolor lorem ipsum dolor",
            "lorem ipsum dolor",
            "lorem ipsum dolor lorem ipsum dolorlorem ipsum dolorlorem ipsum dolorlorem",
            "lorem ipsum dolor",
            "lorem ipsum dolor",
            "lorem ipsum dolor"
        ];

        return(
            <div class="contactBlock">
                <div className="contactInfo">
                    <div className="contactName">лоремова</div>
                    <div className="contactSurname">ипсума анатольевна</div>
                    <div className="contactAdditionInfo">lorem ipsum dolor</div>
                    <div className="hideButtonBlock">
                        <div className="hideButton">Скрыть</div>
                    </div>
                    {contactAchieveList.map((value) => (
                        <div className="contactAchieveLine">
                            <div className="achieveMark"/>
                            <div className="achieveText">{value}</div>
                        </div>
                    ))}
                    <div className="contactDescriptionTitle descriptionText">lorem ipsum dolor</div>
                    <div className="contactDescriptionText descriptionText">
                        lorem ipsum dolorlorem ipsum dolorlorem ipsum dolorlorem ipsum dolorlorem ipsum dolorlorem ipsum dolorlorem ipsum dolorlorem ipsum dolorlorem ipsum dolorlorem ipsum dolor
                    </div>
                    <div className="secondContactDescriptionTitle descriptionText">lorem ipsum dolor</div>
                    <div className="secondContactDescriptionText descriptionText">
                        lorem ipsum dolor lorem ipsum dolor
                    </div>
                </div>
                <img src={image} className="contactImage" alt="contactImage"/>
            </div>
        );
    }
}