import React from "react";
import {ReactComponent as LogoIcon} from '../../images/Header/LOGO.svg';
import {ReactComponent as PhoneIcon} from '../../images/Header/PhoneIcon.svg';
import {ReactComponent as InstagramIcon} from '../../images/Header/instagramIcon.svg';
import {ReactComponent as VkIcon} from '../../images/Header/vkIcon.svg';
import {ReactComponent as MenuIcon} from '../../images/Header/menuButton.svg';

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isMenuOpen: false,
        };
        this.changeMenuBackGround = this.changeMenuBackGround.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
    }

    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }

    handleScroll() {
        const {isMenuOpen} = this.state;
        if(!isMenuOpen) {
            if (window.pageYOffset > 5) {
                document.getElementById("mainWrapHeader").style.backgroundColor = "#ffffff";
                document.getElementById("headerPhoneSupport").style.color = "#000000";
                for (let i = 1; i <= 6; i++) {
                    let str = "logoIconWords" + i;
                    document.getElementById(str).style.fill = "#000000";
                }
            } else {
                for (let i = 1; i <= 6; i++) {
                    let str = "logoIconWords" + i;
                    document.getElementById(str).style.fill = "#ffffff";
                }
                document.getElementById("mainWrapHeader").style.backgroundColor = "transparent";
                document.getElementById("headerPhoneSupport").style.color = "#ffffff";
            }
        }
    }


    changeMenuBackGround() {
        const {openMenu} = this.props;
        const {isMenuOpen} = this.state;
        if(isMenuOpen) {
            const firstLine = document.getElementById("logoIconWords5");
            firstLine.style.transformOrigin = "50% 50%";
            firstLine.style.transform = "translateY(0) rotate(0)";
            const secondLine = document.getElementById("logoIconWords6");
            secondLine.style.transformOrigin = "50% 50%";
            secondLine.style.transform = "translateY(0) rotate(0)";

            for (let i = 1; i <= 6; i++) {
                let str = "logoIconWords" + i;
                document.getElementById(str).style.fill = "#ffffff";
            }
            document.getElementById("mainWrapHeader").style.backgroundColor = "transparent";
            document.getElementById("headerPhoneSupport").style.color = "#ffffff";
        } else {
            const firstLine = document.getElementById("logoIconWords5");
            firstLine.style.transformOrigin = "50% 50%";
            firstLine.style.transform = "translateY(5px) rotate(45deg)";
            const secondLine = document.getElementById("logoIconWords6");
            secondLine.style.transformOrigin = "50% 50%";
            secondLine.style.transform = "translateY(-4px) rotate(-45deg)";

            document.getElementById("mainWrapHeader").style.backgroundColor = "#ffffff";
            document.getElementById("headerPhoneSupport").style.color = "#000000";
            for (let i=1; i<=6; i++) {
                let str = "logoIconWords" + i;
                document.getElementById(str).style.fill = "#000000";
            }
        }
        this.setState({
            isMenuOpen: !isMenuOpen,
        });
        openMenu();
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }

    render() {
        return(
            <div id="mainWrapHeader" className="mainWrapHeader">
                <div className="contentBlock">
                    <div className="logoBlock">
                        <LogoIcon />
                    </div>
                    <div className="contactsBlock">
                        <div className="contactsBlockDesktop">
                            <PhoneIcon style={{margin: "0 19px 0 0"}}/>
                            <a href="tel:+4999999999" className="headerPhoneSupport">
                                <span id="headerPhoneSupport">8 499 999 99 99</span>
                            </a>
                            <InstagramIcon style={{margin: "0 28px 0 0"}}/>
                            <VkIcon style={{margin: "0 56px 0 0"}}/>
                        </div>
                        <MenuIcon className="menuButton" onClick={this.changeMenuBackGround}/>
                    </div>
                </div>
            </div>
        );
    }
}